export EDITOR=nvim
export TERMINAL=termite
export FZF_DEFAULT_OPTS="--height 40% --layout=reverse --border"
export INPUTRC="~/.config/readline/inputrc"
export SUDO_ASKPASS=/usr/lib/ssh/x11-ssh-askpass

# Get the environment of this LINRICE installation
# Variable readed in some configuration files
if [ -f /etc/linrice_env ]; then
   export LINRICE_ENV=$(cat /etc/linrice_env)
fi

mpd &> /dev/null &
