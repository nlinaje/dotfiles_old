" File: .vimrc
" Description: config file for vim

" Avoid error messages when starting vi (not vim) or vim.tiny while
" reading .vimrc
if version >= 500
" General settings ------------------- {{{
   " Enable vim enhancements and improvements (do not be compatible with vi)
   " Note: If .vimrc exists, it is already set to nocompatible. Inserted
   " here to be safe in case it changes or run with a parameter that disables it
   " like using (vim -u <vimrc_file)
   set nocompatible
   " Set the leader key to comma ','
   let mapleader = ","
   " Set all buffers to hidden. Allow to switch buffer without saving file first
   set hidden
   " Set encoding to UTF-8
   set encoding=utf-8
   " Visual autocomplete for command menu
   set wildmenu
   " List all completions on first tab to longest common - follow menu entries
   " in successive tab keys for full completion
   set wildmode=list:longest,full
   " List buffers and prepare to jump to desired buffer
   nnoremap <leader>b :ls<cr>:b<space>
   " Jump directly to alternate buffer (only because it is easier in german
   " keyboard layout as the alternate key <C-6> as <C-^> does not work)
   nnoremap <leader>ab <C-^>
   " ESC key is too far away. Use jk instead to switch from insert
   " to normal mode. Remap ESC key to no operation
   inoremap jk <esc>
   inoremap <esc> <nop>
   " Edit VIMRC
   nnoremap <leader>ve :vsplit $MYVIMRC<cr>
   " Source VIMRC
   nnoremap <leader>vs :source $MYVIMRC<cr>
   " Save more history for ex commands (:, /, <C-r>=, ...)
   " There are 5 separated history tables (See :h history)
   set history=200 " (default =20)
   " Toggle paste mode (disable automated indenting,...)
   set pastetoggle=<leader>z
   " Update file (no write) with <leader>w
   nnoremap <leader>w :update<cr>
   " Quit without saving (will not quit if not explicit saved before)
   nnoremap <leader>q :quit<cr>
   " Execute script in a shell without exiting vim
   nnoremap <leader>x :update<cr>:!clear;./%<CR>
   " Execute shellcheck in a shell without exiting vim
   nnoremap <leader>s :Errors<CR>
   " Delete all spaces at the end of a line (whole file)
     " * Save current contents of "/ (register) in variable
     " * <Bar> == | (pipe -> next command)
     " * Search "\s" (whitespace chars - space + tabs) until end of line
     "   and substitute with empty char (flag: e - do not issue error if no found)
     " * Restore contents of "/ register
   nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>
   " Add all subdirectories to path to use with :find command
   set path+=$PWD/**
   " Do no redraw in middle of macros and other commands (lazyredraw)
   " Do not use it: status line wont immediately appear until a key is pressed
   "set lazyredraw
   " Default Tabs/Shift expansion configuration
   set tabstop=3 shiftwidth=3 expandtab
   " Default horizontal/vertical split below/right current window
   set splitbelow
   set splitright
   " Expand current directory to open a new buffer with edit/split/vsplit/tab
   nnoremap <leader>ew :edit <C-R>=expand("%:p:h")."/"<CR>
   nnoremap <leader>es :split <C-R>=expand("%:p:h")."/"<CR>
   nnoremap <leader>ev :vsplit <C-R>=expand("%:p:h")."/"<CR>
   nnoremap <leader>et :tabedit <C-R>=expand("%:p:h")."/"<CR>
   " Allow saving a file as sudo when user forgets to open the file using sudo
   cnoremap w!! w !sudo tee > /dev/null %
   " Copy visual selection to xclip (copy to register 'y' and sent the
   " register to xclip)
   vnoremap <silent><Leader>y "yy <Bar> :call system('xclip -sel clipboard', @y,)<CR>
" }}}
" Miscelaneous ----------------------- {{{
   " Open page in firefox browser
   nnoremap <leader>fb :up<cr>:silent !firefox %:p &<cr>:redraw!<cr>
   " Open page in chrome browser
   nnoremap <leader>cb :up<cr>:silent !google-chrome %:p &<cr>:redraw!<cr>
   " Delete comment character when joining commented lines
   if v:version > 703 || v:version == 703 && has("patch541")
      set formatoptions+=j
   endif
   " Map %% to %:h to expand directory of current buffer in Ex mode
   " You can use it like this :edit %% => and it will expand it automatically
   " It is the same as :edit %:h<tab>
   cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
" }}}
" Appearance ------------------------- {{{
   " Colors ---------------------- {{{
   " Enable 256 colors in vim (vim.wikia.com/wiki/256_colors_in_vim)
   set t_Co=256
   " Colorize column 80
   set colorcolumn=80
   " Enable syntax highlight (source syntax.vim)
   syntax enable
   " Colorscheme
   hi clear
   syntax reset
   " gruvbox colorscheme from https://github.com/morhetz/gruvbox
   " save gruvbox.vim file in ~/.vim/colors
   " you can use it with FantasqueSansMono font
   " https://github.com/belluzj/fantasque-sans
   " Do not set italic - colorscheme does not work correctly if tmux activated
   " let g:gruvbox_italic=1
   set background=dark
   colorscheme gruvbox
   syntax enable
   " Change highlighting groups (:h highlight-groups) - colors should be
   " changed after colorscheme definition
   " Highlight current line or draw a horizontal line depending on color scheme
   set cursorline

   " Colors for the tab line - Do not use the ones from gruvbox
   " Set tab color for selected tab
   hi TabLineSel ctermfg=232 ctermbg=231
   " Set tab color for tabs that are not selected
   hi TabLine ctermfg=255 ctermbg=235
   " Set tab color for the background from last tab till end of line
   hi TabLineFill ctermfg=255 ctermbg=235

   nnoremap <F3> :call ToggleBackground()<CR>
   let g:scheme_bg = "dark"
   function! ToggleBackground()
      if g:scheme_bg == "dark"
         set background=light
         let g:scheme_bg = "light"
      else
         set background=dark
         let g:scheme_bg = "dark"
      endif
   endfunction
   " }}}
   " Invisible characters -------- {{{
   " Toggle displaying 'invisible characters' like end of line/tabs/spaces...
   nnoremap <leader>l :set list!<cr>
   " Use other chars as list characters
   set listchars=tab:▸\ ,eol:¬
   " Change the colors of the invisible chars when displayed
   highlight NonText guifg=#4a4a59
   highlight SpecialKey guifg=#4a4a59
   " }}}
" }}}
" Movements -------------------------- {{{
"TODO
   " Navigate between splits (CTRL+ hjkl) without entering <C-W> first
   nnoremap <C-H> <C-W><C-H>
   nnoremap <C-J> <C-W><C-J>
   nnoremap <C-K> <C-W><C-K>
   nnoremap <C-L> <C-W><C-L>
   " Jumplist (:jumps, <C-O>, <C-I>)
   " Ctag list
   " Changes (:changes, g;, g,)
   nnoremap <F4> :!~/.git_template/hooks/ctags
                 \ && echo "ctags executed"
                 \ \|\| echo -en "Command ctags failed.\nIf you are not using
                 \ a git directory try: ':\!ctags -R'\nInstall ctags in case
                 \ not done yet."<CR>
   " Remap next difference ([c) with sth easier for german keyboard (<c)
   nnoremap <silent> <c [c<cr>
   " Remap previous difference (]c) with sth easier for german keyboard (<p)
   nnoremap <silent> <p ]c<cr>
   " Location list
   " Buffer list
   nnoremap <silent> <bn :bnext<cr>
   nnoremap <silent> <bp :bprevious<cr>
   nnoremap <silent> <bf :bfirst<cr>
   nnoremap <silent> <bl :blast<cr>
   " Arglist
   nnoremap <silent> <an :next<cr>
   nnoremap <silent> <ap :previous<cr>
   nnoremap <silent> <af :first<cr>
   nnoremap <silent> <al :last<cr>

   " Move current line in Normal mode Up/Down with Alt+Up or Alt+Down
   nnoremap [1;3B :m .+1<CR>==
   nnoremap [1;3A :m .-2<CR>==

   " Move current line in Insert mode Up/Down with Alt+Up or Alt+Down
   inoremap [1;3B <Esc>:m .+1<CR>==gi
   inoremap [1;3A <Esc>:m .-2<CR>==gi

   " Move selected lines in Visual mode Up/Down with Alt+Up or Alt+Down
   vnoremap [1;3B :m '>+1<CR>gv=gv
   vnoremap [1;3A :m '<-2<CR>gv=gv


" }}}
" Sessions --------------------------- {{{
   " Save all sessions in ~/vim-sessions directory
   let g:sessions_dir = '~/vim-sessions'
   " Save a session - user must give the name of the file
   exec 'nnoremap <Leader>ss :mksession! ' . g:sessions_dir .
      \ '/*.vim<C-D><BS><BS><BS><BS><BS>'
   " Restore a session - user must give the name of the file
   exec 'nnoremap <Leader>sr :source ' . g:sessions_dir.
      \ '/*.vim<C-D><BS><BS><BS><BS><BS>'
   " Display the current session name
   exec 'nnoremap <Leader>sf :echo "Session filename:" v:this_session<cr>'
" }}}
" Searching -------------------------- {{{
   " When using f/F/t/T ; and , are used to repeat - add ö and ä because they
   " are easier in german keyboard (do not remove default ones)
   nnoremap ö ;
   nnoremap ä ,
   " When searching and using the repeat commands n/N the cursor may be put at
   " any line of the current window and you must search the cursor - use zz
   " after each repetition to have the cursor always in the middle
   nnoremap n nzz
   nnoremap N Nzz
   " Ignore case when searching (use smartcase instead ignorecase)
   "set ignorecase
   " Smart Case sensitive search :
   "    /abc (insensitive),
   "    /Abc (case sensitive),
   "    /abc\C (case sensitive),
   "    /Abc\c (case insensitive)
   set smartcase
   " Show where the pattern matches as it is being type while searching
   set incsearch
   " Highlight all matches of a search
   set hlsearch
   " Highlight matching [], {}, ()
   set showmatch
   " Toggle search highlighting on/off
   nnoremap <leader><space> :set hlsearch!<cr>
   " Search word under cursor in all files found in current $pwd
   nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:botright cwindow<CR>
   " Open Quick Fix Window automatically if a command (i.e. grep) inserted
   " anything
   augroup quickfix_window
      autocmd!
      autocmd QuickFixCmdPost [^l]* botright cwindow
      autocmd QuickFixCmdPost l*    botright lwindow
   augroup END
   " Map \ to grep or ag - open quickfix window with found items
   if executable('ag')
      " Use ag over grep (global for vim - using grep command)
      set grepprg=ag\ --nogroup\ --nocolor
      " Define new command Ag (see :h command and command-nargs)
      command! -nargs=+ -complete=file -bar Ag silent! grep! <args>|redraw!|cwindow
      nnoremap \ :Ag -i<space>
   else
      " Define new command grep
      command! -nargs=+ -complete=file -bar Grep silent! grep! <args>|redraw!|cwindow
      nnoremap \ :Grep -RnsiI<space>
   endif
" }}}
" Line numbering --------------------- {{{
  " Per default we have number and relativenumber
  " If we loose focus, disable relativenumber (but still number)
  let g:MyEnableAU=1
  augroup numbertoggle
     autocmd!
     autocmd BufEnter,FocusGained * if g:MyEnableAU |
             \ set number relativenumber | endif
     autocmd BufLeave,FocusLost * if g:MyEnableAU |
             \ set number norelativenumber | endif
  augroup END
  " Set absolute numbers
  nnoremap <leader>na :set number norelativenumber<cr>:let MyEnableAU=1<cr>
  " Set relative numbers
  nnoremap <leader>nr :set nonumber relativenumber<cr>:let MyEnableAU=1<cr>
  " Set hybrid numbers
  " No numbers
  nnoremap <leader>nh :set number relativenumber<cr>:let MyEnableAU=1<cr>
  nnoremap <leader>nn :set nonumber norelativenumber<cr>:let MyEnableAU=0<cr>
" }}}
" Markdown --------------------------- {{{
  " Source Markdown File to PDF (Update it + convert to PDF + open PDF in evince)
  " Note : % (filename), :t (tail - or last path component only), :r (root - or
  " one extension removed) - see :help expand and
  " vim.wikia.com/wiki/Get_the_name_of_the_current_file
  " Examples : echo @%, echo expand('%:t') - only file without path,
  " echo expand('%:t:r') - only file without path and without extension
  nnoremap <leader>cp :up<cr>:!pandoc % -o %:t:r.pdf<cr>:!evince %:t:r.pdf<cr>
  " Convert Markdown File to DOCX (Update it + convert to DOCX)
  nnoremap <leader>cw :up<cr>:!pandoc % -o %:t:r.docx<cr>

" }}}
" Resizing --------------------------- {{{
  " Resize current window with F8/F9 (decrease/increase horizontal 5)
  " Resize current window with <shift>F8/F9 (decrease/increase vertically 5)
  " Standard keystrokes [count](Hor:<C-W> +, <C-W> -, Vert: <C-W> <, <C-W> >)
  "                     (All windows the same width/height: <C-W> =)
  nnoremap <silent> <F8> :exe "resize -5"<cr>
  nnoremap <silent> <F9> :exe "resize +5"<cr>
  nnoremap <silent> <S-F8> :exe "vertical resize -5"<cr>
  nnoremap <silent> <S-F9> :exe "vertical resize +5"<cr>
" }}}
" Netrw settings --------------------- {{{
  " Open netrw in a link window with width 25
  nnoremap <leader>fm :25vs .<cr>
  " Remove "header spam" in directory mode
  let g:netrw_banner = 0
  " Style = tree(3) - Other styles (0:thin,1:long,2:wide)
  let g:netrw_liststyle = 3 "Tree style
" }}}
" Changes in file -------------------- {{{
   nnoremap <leader>vc :changes<cr>
   " Got to next change
   nnoremap <leader>n g;
   " Got to previous change
   nnoremap <leader>p g,
   " Highlight the text (visual mode) you entered last time in Insert Mode
   " and remain in visual mode
   nnoremap <leader>V `[v`]
" }}}
" Filetypes -------------------------- {{{
   " In markdown let use fenced code syntax highlighting
   " Use it like this: ```<language> , i.e. ```vim or ```bash
   let g:markdown_fenced_languages = ['html', 'vim', 'ruby', 'python', 'sql',
                                     \'bash=sh', 'sqloracle', 'git', 'java',
                                     \'javascript', 'gitconfig', 'cpp', 'perl' ]
   " Define Badwhitespace color
   highlight BadWhitespace ctermbg=red guibg=darkred
   augroup filetypes
      autocmd!
      autocmd BufNewFile,BufReadPost *.md set filetype=markdown
      autocmd BufNewFile,BufReadPost *.csv,*.dat set filetype=csv
      " Comment files depending on its type
      autocmd Filetype c,cpp,java,go,scala,javascript let b:comment_leader='\/\/'
      autocmd Filetype sh,ruby,python                 let b:comment_leader='#'
      autocmd Filetype vim                            let b:comment_leader='\"'
      " Local properties depending on filetype
      autocmd FileType vim setlocal foldmethod=marker
      " All files should display bad white spaces at the end of lines
      " See color definition
      autocmd BufNewFile,BufReadPost * match BadWhitespace /\s\+$/
      " When editing a file, always jump to the last cursor position
      autocmd BufReadPost *
         \ if line("'\"") > 0 && line ("'\"") <= line("$") |
         \ exe "normal! g'\"" |
         \ endif
      " PEP 8 identation standard - use the same for bash/markdown
      autocmd BufNewFile,BufRead *.py,*.sh,*.md
         \ set tabstop=4 |     " number visual spaces per TAB
         \ set softtabstop=4 | " number of spaces in tab when editing
         \ set shiftwidth=4 |  " number spaces when indenting with operators
         \ set expandtab |     " turn tabs into spaces
         \ set autoindent |
         \ set fileformat=unix
      " For txt/md files use textwidth=80
      autocmd BufNewFile,BufRead *.md
         \ set textwidth=80  " maximum chars per line before wrapping
   augroup END

   " Toggle comment lines in normal/visual mode - comment_leader var must be
   " assigned depending on filetype
   function! ToggleComment()
   \" help with :h \v or pattern-atoms
       if exists('b:comment_leader')
           if getline('.') =~ '\v^\s*' .b:comment_leader
           \" uncomment the line
               execute 'silent s/\v^\s*\zs' .b:comment_leader.'[ ]?//g'
           else
       \" comment the line
               execute 'silent s/\v^\s*\zs\ze(\S|\n)/' .b:comment_leader.' /g'
           endif
       else
           echo 'no comment leader found for filetype'
       end
   endfunction
   noremap <silent> <leader>c :call ToggleComment()<cr>
" }}}
" VIM Plugins ------------------------ {{{
   " Before loading plugins some commands are required
   filetype off
   " set the runtime path to include Vundle and initialize
   "set rtp+=~/.vim/bundle/Vundle.vim
   set rtp+=/usr/share/vim/vimfiles/autoload/vundle.vim
   call vundle#begin()
  Plugin 'vim-syntastic/syntastic' " (:h syntastic - Check syntax errors)
  Plugin 'ctrlpvim/ctrlp.vim'      " (:h ctrlp - Fuzzy file,buffer,mru finder)
"  Plugin 'mattn/emmet-vim'         " (:h emmet - HTML/CSS abbrev expansion)
"  Plugin 'majutsushi/tagbar'       " (:h tagbar - Tag buffer in memory)
"  Plugin 'vim-scripts/dbext.vim'   " (:h dbext - SQL directly in VIM)
  Plugin 'Yggdroot/indentline'     " (:h indentline - Show indentation chars)
  Plugin 'tpope/vim-fugitive'      " (:h fugitive - Git Wrapper)
  Plugin 'tpope/vim-surround'      " (:h surround - Surround words)
  Plugin 'airblade/vim-gitgutter'  " (:h gitgutter - show modified lines)
  Plugin 'tmhedberg/SimpylFold'    " (:h SimpylFold - Simple Fold in Python)
"  Plugin 'chrisbra/csv.vim'        " (:h ft-csv - Handle CSV files)
"  " Plugin 'Valloric/YouCompleteMe'  " (:h youcompleteme - Display completes)
"  Plugin 'SirVer/ultisnips'        " (:h ultisnips - Snippets in Vim)
"  Plugin 'honza/vim-snippets'      " (See ~/.vim/bundle/vim-snippets/snippets/)
   call vundle#end()
   " Plugins are loaded - set filetype, plugin and indent to on again
   filetype plugin indent on
   " Plugin tweaks --------------- {{{
      " Emmet VIM --------------- {{{
      " TODO - check Change leader emmet key to tab (instead of <c-y>)
      let g:user_emmet_leader_key='<C-y>'
      " let g:user_emmet_leader_key='<Tab>'
      " }}}
      " CTRL_P ------------------ {{{
      " Ctrl-p window: Order files from top to bottom
      let g:ctrlp_match_window = 'bottom,order:ttb'
      " Add extensions to ctrlp plugin
      let g:ctrlp_extensions = ['tag', 'buffertag', 'quickfix', 'dir', 'rtscript',
                             \ 'undo', 'line', 'changes', 'mixed', 'bookmarkdir']
      " Open multiple files in hidden buffers and jump to first one
      let g:ctrlp_open_multiple_files = '1r'
      " Use vim's wildignore to ignore temporary files/binary files
      set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.iso
      " Use The Silver Searcher in CTRL-P if installed
      if executable('ag')
         " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
         let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
         " ag is fast enough that CtrlP doesn't need to cache (TODO test this)
	 "let g:ctrlp_use_caching = 0
      endif
      " }}}
      " Tagbar ------------------ {{{
      " TODO - check if tagbar plugin is needed
      nnoremap <F6> :TagbarToggle<CR>
      " }}}
      " Indentline -------------- {{{
      " Insert a different type of line for each indentation level
      let g:indentLine_char_list = ['⎸', '|', '¦', '┆', '┊', '⋮',
			      \'⸾', '‖','⫯', '⫰','⍿' ]
      " }}}
      " SimpylFold -------------- {{{
      " Display a preview text of the docstring when folded
      let g:SimpylFold_docstring_preview = 1
      " }}}
      " CSV        -------------- {{{
      " Highlight the column where the cursor is placed
      let g:csv_highlight_column='y'
      " }}}
      " Ultisnips --------------- {{{
      " To expand the snippet use <tab> - be careful with other Plugins which
      " may need this key too (emmet, YouCompleteMe...)
      "let g:UltiSnipsExpandTrigger="<tab>"
      let g:UltiSnipsExpandTrigger="<c-j>"
      " Get a list of possible snippets (Not working - see :help UltiSnips)
      let g:UltiSnipsListSnippets="<c-l>"
      " Move to next trigger
      let g:UltiSnipsJumpForwardTrigger="<c-j>"
      " Move to previous trigger
      let g:UltiSnipsJumpBackwardTrigger="<c-k>"

      " If you want :UltiSnipsEdit to split your window.
      let g:UltiSnipsEditSplit="vertical"
      " Private snippets directory
      let g:UltiSnipsSnippetsDir="~/.vim/priv_snippets"
      let g:UltiSnipsSnippetDirectories=['Ultisnips',$HOME.'/.vim/priv_snippets']
      " Split window with snippets

      " }}}
      " YouCompleteMe ----------- {{{
      " Use <C-n> to go through the complete list
"      autocmd CmdwinEnter * inoremap <expr><buffer> <TAB>
"               \ pumvisible() ? "\<C-n>" : "\<TAB>"
      " UltiSnips completion function that tries to expand a snippet. If there's no
      " snippet for expanding, it checks for completion window and if it's
      " shown, selects first element. If there's no completion window it tries to
      " jump to next placeholder. If there's no placeholder it just returns TAB key
      function! g:UltiSnips_Complete()
          call UltiSnips_ExpandSnippet()
          if g:ulti_expand_res == 0
              if pumvisible()
                  return "\<C-n>"
              else
                  call UltiSnips_JumpForwards()
                  if g:ulti_jump_forwards_res == 0
                     return "\<TAB>"
                  endif
              endif
          endif
          return ""
      endfunction
      exec "inoremap <silent> " . g:UltiSnipsExpandTrigger . " <C-R>=g:UltiSnips_Complete()<cr>"
      " }}}
      " DBEXT ------------------- {{{
      " Types: ORA (Oracle), MYSQL (MySQL)
      let g:p1_type=substitute(system("grep P1_TYPE /etc/rc.config"),"\n","","")
      if g:p1_type ==? "P1_TYPE=\"ITAPSERVER\""
         let g:dbext_default_profile_slot1 = 'type=ORA:user=build_slot_1@icas:passwd=build_slot_1'
         let g:dbext_default_profile_slot2 = 'type=ORA:user=build_slot_2@icas:passwd=build_slot_2'
         let g:dbext_default_profile_slot3 = 'type=ORA:user=build_slot_3@icas:passwd=build_slot_3'
         let g:dbext_default_profile_slot4 = 'type=ORA:user=build_slot_4@icas:passwd=build_slot_4'
         let g:dbext_default_profile_slot5 = 'type=ORA:user=build_slot_5@icas:passwd=build_slot_5'
         " let g:dbext_default_ORA_cmd_header = "set timing on\n" .
                  " \ "set autotrace on\n" .
                  " \ "set pagesize 50000ņ" .
                  " \ "set wrap off\n" .
                  " \ "set sqlprompt \"\"\n" .
                  " \ "set linesize 10000\n" .
                  " \ "set flush off\n" .
                  " \ "set colsep \"   \"\n" .
                  " \ "set tab off\n\n"
         let g:dbext_default_profile = 'slot1'
         let s:current_profile_number = 0
         let s:dbext_profiles = ['slot1', 'slot2', 'slot3', 'slot4', 'slot5']
         " Toggle between profiles (Up/down - from 1 to 5)
         function! Toggle_dbext_profile(step)
            " Apply the given step (+1 or -1)
            let s:current_profile_number = s:current_profile_number + a:step
            " Reset current_profile_number if outside array
            if s:current_profile_number >= len(s:dbext_profiles)
               let s:current_profile_number = 0
            endif
            if s:current_profile_number < 0
               let s:current_profile_number = len(s:dbext_profiles) - 1
            endif
            let l:exec_string = ':DBSetOption profile=' .
                     \s:dbext_profiles[s:current_profile_number]
            execute l:exec_string
            echo "Current schema: " . s:dbext_profiles[s:current_profile_number]
         endfunction
         nnoremap <leader>db :call Toggle_dbext_profile(1)<CR>
         nnoremap <leader>dB :call Toggle_dbext_profile(-1)<CR>
         nnoremap <leader>sst :Select table_name from user_tables order by table_name;<CR>
      elseif g:p1_type ==? "P1_TYPE=\"DISSERVER\""
         let g:dbext_default_profile_disserver = 'type=MYSQL:user=root:passwd=:dbname=disserver'
         let g:dbext_default_profile = 'disserver'
         nnoremap <leader>sst :DBExecSQL show tables;<CR>
      endif
      " }}}
   " }}}
   " Plugin documentation -------- {{{
   " Brief help for Vundle
   " 1 - Update .vimrc with all Plugins
   " 2 - Save + source .vimrc
   " 3 - Install (:PluginInstall)
   " Commands :
   " :PluginList       - lists configured Plugins
   " :PluginInstall    - installs plugins; append `!` to updade
   " :PluginUpdate     - update plugins (or :PluginInstall!)
   " :PluginSearch foo - searches for foo; append `!` for refresh local cache
   " :PluginClean      - confirms removal of unused plugins; append `!`
   "                     to auto-approve removal
   " :h vundle         - for more details
   " Ctags with git:
   " https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html
   " Emmet VIM - support for expanding abbreviations
     " Tutorials: (vim emmet and original emmet for other editors)
     " https://github.com/mattn/emmet-vim/blob/master/TUTORIAL.mkd
     " https://docs.emmet.io/abbreviations/syntax/
   " Snippets tutorial:
   " https://developpaper.com/vim-code-snippet-plug-in-ultisnips-usage-tutorial/
   " }}}
" }}}
" Folding ---------------------------- {{{
" TODO
   " enable folding with the spacebar
   nnoremap <space> za
" }}}
" Status line ------------------------ {{{
   hi BuffNFileTypeColor ctermfg=255 ctermbg=240
   hi FilePathColor ctermfg=232 ctermbg=231
   hi LineColColor ctermfg=222 ctermbg=237
   hi FlagsColor ctermfg=255 ctermbg=196
   hi StatusColor ctermfg=222 ctermbg=064

   " Change Color "StatusColor" depending on the current mode (insert,normal...)
   function! ChangeStatuslineColor()
      " Comparisons (==# is case sensitive, ==? is case insensitive)
      if (mode() ==# 'n')
         " Normal mode
         exe 'hi! StatusColor ctermfg=232 ctermbg=231'
      elseif (mode() =~? 'v' || mode() =~? '')
         " Visual Mode
         exe 'hi! StatusColor ctermfg=232 ctermbg=223'
      elseif (mode() ==# 'i')
         " Insert Mode
         exe 'hi! StatusColor ctermfg=255 ctermbg=196'
      elseif (mode() ==# 'R')
         " Replace Mode
         exe 'hi! StatusColor ctermfg=232 ctermbg=220'
      else
         " All other modes
         exe 'hi! StatusColor ctermfg=223 ctermbg=032'
      endif
      return ''
   endfunction

   set laststatus=2
   set statusline=                                   " Reset Status line
   set statusline+=%#BuffNFileTypeColor#             " Change color
   set statusline+=[%n]                              " Buffer number
   set statusline+=%#FilePathColor#\                 " Change color
   set statusline+=%<%F\                             " File+path
   set statusline+=%#LineColColor#\                  " Change color
   set statusline+=%=\                               " Start right alignment
   set statusline+=line:%l/%L\ (%03p%%)\             " Lines current/total (%)
   set statusline+=col:%03c\                         " Column Number
   set statusline+=%y\                               " FileType
"   set statusline+=(%03b\ d/0x%02B)                  " Char value decimal/hex
   set statusline+=%#FlagsColor#                     " Change color
   set statusline+=%m                                " Modified flag
   set statusline+=%r                                " Readonly flag
   set statusline+=%w                                " Preview flag
   set statusline+=%h                                " Help flag
   set statusline+=%#StatusColor#\                   " Change color
   set statusline+=%{toupper(g:currentmode[mode()])} " Current mode
   set statusline+=%{ChangeStatuslineColor()}        " Call ChangeStatuslineColor

   " Reference: https://gabri.me/blog/diy-vim-statusline
   " Added  and ' (CTRL-V-V/S) as it was not working
   " Dictionary mode() => String in Status Line
   let g:currentmode={
       \ 'n'      : 'Normal ',
       \ 'no'     : 'N·Operator Pending ',
       \ 'v'      : 'Visual ',
       \ 'V'      : 'V·Line ',
       \ '\<C-V>' : 'V·Block ',
       \ ''     : 'V·Block ',
       \ 's'      : 'Select ',
       \ 'S'      : 'S·Line ',
       \ '\<C-S>' : 'S·Block ',
       \ ''     : 'S·Block ',
       \ 'i'      : 'Insert ',
       \ 'R'      : 'Replace ',
       \ 'Rv'     : 'V·Replace ',
       \ 'c'      : 'Command ',
       \ 'cv'     : 'Vim Ex ',
       \ 'ce'     : 'Ex ',
       \ 'r'      : 'Prompt ',
       \ 'rm'     : 'More ',
       \ 'r?'     : 'Confirm ',
       \ '!'      : 'Shell ',
       \ 't'      : 'Terminal ',
       \}
" }}}
" Help  ------------------------------ {{{
   " Add the man plugin which comes with vim
   runtime! ftplugin/man.vim
   " Show vim help from word under cursor
   nnoremap <leader>h :help <C-R><C-W><cr>
   " Show Linux manual in vim from word under cursor
   nnoremap <leader>m :Man <c-R><C-W><cr>
" }}}
" Local environment ------------------ {{{
   " Local local environment if it exists
   if filereadable(expand('~/.localenv/vimrc.local'))
      source ~/.localenv/vimrc.local
   endif

" }}}
endif
" References ------------------------- {{{
" Vim Help
" :help
" Books:
" * VIM the Hardway by Steve Losch
"     Link: https://learnvimscriptthehardway.stevelosh.com
" * Practical VIM by Drew Neil
"     Link: https://pragprog.com/book/dnvim2/practical-vim-second-edition
" * VIM Users Guide (VIM Tutorial)
"     Link: ftp://ftp.vim.org/pub/vim/doc/book/vimbook-OPL.pdf
" * 10 minute exercises by Steve Shogrem
"     Link: http://leanpub.com/deliberatevim
" * VIM 101 hacks by Ramesh Natarajan
"    Link: https://www.thegeekstuff.com/vim-101-hacks-ebook
" Internet addresses:
" * https://dougblack.io/words/a-good-vimrc.html
" * https://vimgolf.com
" * https://stackoverflow.com
" Other resources:
" * https://github.com/learnbyexample/scripting_course/blob/master/Vim_curated_resources.md
" * vim.wikia.com/wiki/Get_the_name_of_the_current_file
" * https://vim.fandom.com/wiki/Resize_splits_more_quickly
" * https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/
" And many many others....
" }}}
" vim:foldmethod=marker:foldlevel=0

" TODO:
" * Snippets for markdown
" * Colors for markdown
" * Look for some tweaks for markdown in vim
" grep, ngrep
" * Line info
" TODO: textwidth only for txt and md files => not working
" * Plugins (end of this file)
" * Backup files (See other vimrc files in internent)
" TODO: post-commit hook in .git_templates to:
" * Create a docker RH7 container that creates automatically the RPM file

"TODO: COlors for the different modes
"https://statico.github.io/vim3.html
"http://got-ravings.blogspot.com/2008/08/vim-pr0n-making-statuslines-that-own.html
"https://gabri.me/blog/diy-vim-statusline

"TODO: Minor error:
" * Two windows open in vim - then start insert mode in one of them
" * Both windows change to "Insert" mode
" * If you go to normal mode, the current window changes it, but the other
" continues to display it is in insert mode

"  Plugin 'vim-scripts/indentpython.vim'
"
"  " Check following plugins
"  " Plugin 'vim-ctrlspace/vim-ctrlspace'
"  "Plugin 'bitc/vim-bad-whitespace'
"  "Bundle 'Valloric/YouCompleteMe'
"  " -- Syntax error checks plugin for Python
"  "Plugin 'nvie/vim-flake8'
"  " -- Another status line plugin
"  " Plugin 'godlygeek/tabular'
"  " Plugin 'nathanaelkane/vim-indent-guides'
   " Bash group ------------------ {{{
"   augroup bash_group
"   	autocmd!
"   	autocmd BufNewFile,BufRead *.sh,*.md
"   		\ set tabstop=4 |
"   		\ set softtabstop=4 |
"   		\ set shiftwidth=4 |
"   		\ set textwidth=80 |
"   		\ set expandtab |
"   		\ set autoindent |
"   		\ set fileformat=unix |
"   		\ set number
"   	autocmd BufRead,BufNewFile *.sh match BadWhitespace /\s\+$/
"   augroup END
"
"   " Check following settings for bash
"   "" Folding in bash --------------- {
"   "set foldenable
"   "set foldmethod=marker
"   "au FileType sh let g:sh_fold_enabled=5
"   "au FileType sh let g:is_bash=1
"   "au FileType sh set foldmethod=syntax
"   "syntax enable
"   " }}}
"
"   " Pythong group ---------------------- {{{
"   " PEP 8 identation standard
"   augroup pep8_python_group
"   	autocmd!
"   	autocmd BufNewFile,BufRead *.py
"   		\ set tabstop=4 |
"   		\ set softtabstop=4 |
"   		\ set shiftwidth=4 |
"   		\ set textwidth=80 |
"   		\ set expandtab |
"   		\ set autoindent |
"   		\ set fileformat=unix
"   	autocmd BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
"   " -- Python IDE ideas -- extracted from https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/
"   " Enable folding
"       " autocmd Filetype python set foldmethod=indent set foldlevel=99
"   augroup END
"   " }}}

"   " VIM group -------------------- {{{
"   augroup filetype_vim
"   	autocmd!
"   	autocmd FileType vim setlocal foldmethod=marker
"   augroup END
"   " }}}
"
"   " Markdown group -------------------- {{{
"   augroup filetype_md
"   	autocmd!
"   	autocmd BufNewFile,BufReadPost *.md set filetype=markdown
"   augroup END
"
"   let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
"   " }}}
"
"   " CSV group ---------------------------- {{{
"   augroup filetype_csv
"      autocmd! BufRead,BufNewFile *.csv,*.dat   setfiletype csv
"   augroup END
"   " }}}
"
"   " Others ---------------------------- {{{
"   augroup others
"      " When editing a file, always jump to the last cursor position
"      autocmd BufReadPost *
"         \ if line("'\"") > 0 && line ("'\"") <= line("$") |
"         \ exe "normal! g'\"" |
"         \ endif
"   augroup END
"   " }}}
"
"
function! Github_shellcheck() abort
   if get(getloclist(0, {'winid':0}), 'winid', 0)
      " location list is opened

      let id = matchstr(getline("."), ' \[\zsSC\d\+\ze\]$')
      echo "In function gb:" id

      if id =~# '^SC\d\+$'
         let url = "https://github.com/koalaman/shellcheck/wiki/" . id

         " if !exists('g:loaded_netrw')
            " runtime! autoload/netrw.vim
         " endif
 
         call netrw#BrowseX(url, netrw#CheckIfRemote())

         echo url
      endif
   end
endfunction

nnoremap <silent> <leader>g :call Github_shellcheck()<cr>
   " nnoremap <F3> :call ToggleBackground()<CR>
