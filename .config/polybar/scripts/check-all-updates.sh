#!/bin/sh
#source https://github.com/x70b1/polybar-scripts

updates_arch=$(checkupdates 2>&1)
updates_arch=$(echo "$updates_arch" | wc -l)
updates_aur=$(yay --aur -Qu 2>/dev/null | wc -l)
updates=$((updates_arch + updates_aur))

if [ "$updates" -gt 0 ]; then
    echo "$updates"
else
    echo "0"
fi
