#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Start the same polybar configuration in all monitors
for m in $(polybar --list-monitors | cut -d":" -f1); do
   LAN_IFC=$(ls /sys/class/net | grep ^en | awk '{print $1}')  \
   WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') \
   MONITOR=$m \
   polybar --reload mainbar-bspwm &
done

echo "Bars launched..."
