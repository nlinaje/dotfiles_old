# Check we are not using a ksh shell, to avoid the iTG reading this file, as it may avoid it
# from working correctly.
[ $(ps h -o comm $$) == "ksh" ] && return

# Check if we are in non-interactive mode (ssh user@host "command", scp, sftp, ssh-copy-id)
# For scp, sftp, ssh-copy-id we do not want to read this file => return
# For ssh user@host "command" we read this file and expand the aliases so that we can use all
# functions and aliases of this file.

if [ -z "$PS1" ] # We are in non-interactive mode
then
   # Get the current pid
   pid=$$
   # Get the command used to get here (if command has umask 077; test -d it corresponds to ssh-copy-id)
   command=$(ps h -p $pid -o pid,cmd | grep -v scp | grep -v sftp | grep -v 'umask 077; test -d')
   # If command is empty, it is an non-interactive shell with 'ssh user@host "command"': expand aliases
   # otherwise, return
   [[ $command != "" ]] && shopt -s expand_aliases || return
fi

# Colors (B: Bold, U: Underlined)
RED="\e[31m"
BRED="\e[1;31m"
BURED="\e[1;4;31m"
GREEN="\e[32m"
BGREEN="\e[01;32m"
BUGREEN="\e[01;4;32m"
NORMAL="\e[00m"

# For the user prompt, the colors need additional characters "\[ and \]", or it will have a strange behaviour
PRED="\[\e[31m\]"
PBRED="\[\e[01;31m\]"
PBURED="\[\e[01;4;31m\]"
PGREEN="\[\e[32m\]"
PBGREEN="\[\e[01;32m\]"
PBUGREEN="\[\e[01;4;32m\]"
PNORMAL="\[\e[00m\]"

HOSTNAME=$(hostname)
# Get number of cores
num_cores=$(nproc)
# support for \u added in bash 4.2 (check version >= 4.2)
bash_version=$(echo ${BASH_VERSION%%[^0-9.]*})
# Echo current version and 4.2 and sort it. The first line is the older version
# If the first line is the equal to current version => Current version is older as 4.2
if [ "$bash_version" = "$(echo '$bash_version\n4.2' | sort -V | head -n1)" ]
then
   SKULL="\u2620"
   CHECK_MARK="\u2717"
   CROSS="\u2713"
else
   SKULL="dead"
   CHECK_MARK="ok"
   CROSS="nok"
fi

SUBSYS_TYPE="OTHERS"


# Generic  aliases
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias hh='fc -l -50' # Get last 50 commands in history
alias h='history'    # Get last 50 commands in history
alias r='fc -s'      # Repeat last command
alias hg='history | grep -i' # Search for a word in history
# pss: Search for process in the process list (or use pgrep for just the id). Used sed to change first letter x to [x] in order
#      that grep command is not displayed => ps -ef | grep -i orb -> changed to ps -ef | grep -i [o]rb
alias pss='f_pss'
f_pss() { ps -ef | grep --color -i $(echo $1 | sed "s/^\(.\)/\[\1\]/"); }
alias ll='ls -thor' #
alias lx='ls -lXB'  # sort by extension
alias lc='ls -ltcr' # sort by/show change time, most recent last
alias tree='tree -Cuh'

alias cd='cd_func'
alias ..='cd ..;echo $PWD'
alias .2='cd ../../;echo $PWD'
alias .3='cd ../../../;echo $PWD'
alias .4='cd ../../../../;echo $PWD'
alias mnt='mount | column -t'

alias sprompt='PROMPT_COMMAND="super_prompt"'
alias dprompt='PROMPT_COMMAND="set_prompt"'
alias grep='grep --color'

alias ff='find . -print 2> /dev/null | grep -i' # find file in this directory/subdirectories
alias ffls='find . -print 2> /dev/null | xargs ls -lrt | grep -i $1' # find file and perfrom an ls
alias bu='f_bu'
#alias st='set_title'
alias xcolors='for i in {1..255};do printf "\x1b[38;5;${i}mcolours${i}\n"; done'
alias rcolors='cat $1 | sed "s/\x1B\[[0-9;]*[JKmsu]//g"'

alias gitarchive='_gitarchive'
alias gittag='_gittag'
alias gitdtag='_gitdtag'

f_bu() { cp $@ $@.backup-$(date +%d%b%y_%H%M%S); } # Backups a file ending with .backup-<dayMonthYear_HourMinuteSecond>

alias xcs='xfce4-color-switch'

# Sets the title of the Terminal-Tab with the text given. If no parameters
# given, it displays the Site/Hostname
function set_title() {
   title=$1
   [[ -z $title ]] && title="$(grep icas_site /etc/icas.conf 2> /dev/null | cut -d' ' -f2) $(hostname)"
   echo -en "\033]0;${title}\a"
}

# Display the contents of environment variable PATH line by line
function path() {
   echo $PATH | sed 's/\:/\n/g' | sort
}

# Get the network addresses for the process given as parameter
# The function get the pid's of the processes which have part of its
# name as the parameter given
function pna() {
   echo "COMMAND   PID USER   FD   TYPE             DEVICE  SIZE/OFF     NODE NAME"
   for pid in $(pgrep $1)
   do
      lsof -p $pid | grep -e UDP -e TCP | grep -v localhost
   done
}


function cd_func ()
{
   local x2 the_new_dir adir index
   local -i cnt
   if [[ $1 ==  "--" ]]; then
      dirs -v
      return 0
   fi
   the_new_dir=$1
   [[ -z $1 ]] && the_new_dir=$HOME
   if [[ ${the_new_dir:0:1} == '-' ]]; then
      #
      # Extract dir N from dirs
      index=${the_new_dir:1}
      [[ -z $index ]] && index=1
      adir=$(dirs +$index)
      [[ -z $adir ]] && return 1
      the_new_dir=$adir
   fi
   #
   # '~' has to be substituted by ${HOME}
   [[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"
   #
   # Now change to the new dir and add to the top of the stack
   pushd "${the_new_dir}" > /dev/null
   [[ $? -ne 0 ]] && return 1
   the_new_dir=$(pwd)
   #
   # Trim down everything beyond 11th entry
   popd -n +11 2>/dev/null 1>/dev/null

   #
   # Remove any other occurence of this dir, skipping the top of the stack
   for ((cnt=1; cnt <= 10; cnt++)); do
      x2=$(dirs +${cnt} 2>/dev/null)
      [[ $? -ne 0 ]] && return 0
      [[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
      if [[ "${x2}" == "${the_new_dir}" ]]; then
         popd -n +$cnt 2>/dev/null 1>/dev/null
         cnt=cnt-1
      fi
   done
   return 0
}

# Update known_hosts file for given hostnames
function sshup() {
   hosts=$*
   for host in $hosts
   do
      ip=$(grep '\<'$host'\>' /etc/hosts | awk '{print $1}')
      ssh-keygen -R $host > /dev/null 2>&1
      ssh-keygen -R $ip > /dev/null 2>&1
      ssh-keygen -R $host,$ip > /dev/null 2>&1
      ssh-keyscan $host,$ip >> ~/.ssh/known_hosts 2> /dev/null
      echo "Updated ~/.ssh/known_hosts for hostname $host"
   done
}


# Changes the Linux prompt
function set_prompt() {
   if [[ $EUID == 0 ]]
   then
      PS1="${PBRED}[\u@${HOSTNAME}:\w]${PNORMAL} "
   else
      PS1="[\u@${HOSTNAME}:\w] "
   fi
}

# Linux prompt displaying cores, load average, build/adap slots, date, ...
function super_prompt
{
   pid=""
   # see stackoverflow.com/questions/11987495/linux-proc-loadavg for info of loadavg!!!!
   # Display number of CPUs (Cores)

   # Total mem / free /swap
   memfree=$(grep MemFree /proc/meminfo )
   memtotal=$(grep MemTotal /proc/meminfo )
   memcached=$(grep ^Cached /proc/meminfo )
   memfree=${memfree#MemFree: }
   memfree=${memfree% kB}
   memtotal=${memtotal#MemTotal: }
   memtotal=${memtotal% kB}
   memcached=${memcached#Cached: }
   memcached=${memcached% kB}

   memtotal=$(($memtotal/1024)) # In MB
   memfree=$(($memfree/1024)) # In MB
   memcached=$(($memcached/1024)) # In MB


   memfree_cached=$(($memfree + $memcached))

   #percent=$(echo "$memfree / $memtotal * 100" | bc -l)
   percent=$(($memfree_cached/$memtotal*100))
   percent=${percent%.0*}
   [[ $percent < 40 ]] && meminfo="free: $memfree MB,free+cached: ${PBGREEN}$memfree_cached${PNORMAL} MB,total: $memtotal MB" || meminfo="free: $memfree MB,free+cached: ${PBRED}$memfree_cached${PNORMAL} MB,total: $memtotal MB"

   # loadavg (all)
   typeset loadavg=$(cat /proc/loadavg)
   #loadavg=$(echo ${loadavg%[0-9]*/[0-9]* [0-9]*})
   loadavg=$(echo ${loadavg%* [0-9]*})
   #loadavg=$(cat /proc/loadavg)
   typeset current_time=$(date "+%d/%m/%Y %H:%M:%S")
   pid_dhcp=$(pgrep dhcpd)
   pid_snmp=$(pgrep snmpd)
   pid_ntp=$(pgrep ntpd)
   pid_mysql=$(pgrep -x mysqld)
   [[ -z $pid_ntp ]] && pid="${pid}NTPD:${PBRED}$SKULL${PNORMAL} " || pid="${pid}NTPD:${PBGREEN}$CHECK_MARK($pid_ntp)${PNORMAL} "
   [[ -z $pid_dhcp ]] && pid="${pid}DHCPD:${PBRED}$SKULL${PNORMAL} " || pid="${pid}DHCPD:${PBGREEN}$CHECK_MARK($pid_dhcp)${PNORMAL} "
   [[ -z $pid_snmp ]] && pid="${pid}SNMPD:${PBRED}$SKULL${PNORMAL} " || pid="${pid}SNMPD:${PBGREEN}$CHECK_MARK($pid_snmp)${PNORMAL} "
   [[ -z $pid_mysql ]] && pid="${pid}MYSQLD:${PBRED}$SKULL${PNORMAL}" || pid="${pid}MYSQLD:${PBGREEN}$CHECK_MARK($pid_mysql)${PNORMAL}"
   if [[ $EUID == 0 ]]; then
      PS1="\n[$pid] [${num_cores}] [$loadavg] [$meminfo] [$current_time] \n${PBRED}[\u@${HOSTNAME}:\w]${PNORMAL} "
   else
      PS1="\n[$pid] [${num_cores}] [$loadavg] [$meminfo] [$current_time] \n[\u@${HOSTNAME}:\w] "
   fi
}

PROMPT_COMMAND="set_prompt"

function _gitarchive() {
   package=$1
   if [[ ! -n $package ]]; then
       echo "ERROR: You must give the name of the package as parameter"
       return 1
   fi
   if [[ -z "$(git status --porcelain)" ]]; then
       git archive master --prefix="${package}-$(git describe)/" | \
           gzip > ${package}-$(git describe master).tar.gz
   else
       echo "ERROR: Git local repository not ready. Check that git status" \
            "does not give any files to add/commit"
       return 1
   fi

}

function _gittag() {
   version=$(grep ^VERSION * 2> /dev/null | cut -d= -f 2 | head -1 | sed -e 's/\"//g' -e "s/\'//g")
   if [[ -z $version ]]; then
      echo "Variable VERSION of this project could not be found. You must set if first".
   else
      echo "Setting last commit to '$version'"
      git tag -a $version -m "Version $version" && echo "Done" || echo "Failed"
   fi
}

function _gitdtag() {
   last_tag=$(git tag 2> /dev/null | head -1)
   if [[ -z $last_tag ]]; then
      echo "No tags found to delete."
   else
      read -p "Tag '$last_tag' will be removed. Are you sure? " ans
      case $(echo $ans | tr '[[:lower:]]' '[[:upper:]]') in
         J*|Y*) git tag -d $last_tag && echo "Done" || echo "Failed";;
         *) return 1;;
      esac
   fi
}


function xfce4-color-switch() {
    local dir_schemes="/usr/share/xfce4/terminal/colorschemes"
    if ! [[ -f ${dir_schemes}/${1}.theme ]]; then
        echo "Usage: xcs <color-scheme> or"\
           "xfce4-color-switch <color-scheme>"
        echo "No such colorscheme: '$1'."
        echo "Available schemes under ${dir_schemes} are:"
        for file in $(ls ${dir_schemes}/*.theme)
        do
           basename $file .theme
        done
        return 1
    fi
    local current_dir=$(pwd)
    cd ~/.config/xfce4/terminal
    # Get the name of the current Theme (comparing Colorpalette line)
    palette=$(grep ColorPalette terminalrc)
    local current_theme=$(grep -lxf <(echo $palette) ${dir_schemes}/*theme)
    echo "Switching from '$(basename $current_theme .theme)' to '$1'"
    # strip settings from any themes (extract variables not contained in theme)
    grep -Fxvf <(cat ${dir_schemes}/*.theme) terminalrc > .terminalrc.tmp
    # Add theme variables to temp file
    grep -v -e Name -e Scheme ${dir_schemes}/$1.theme >> .terminalrc.tmp
    # Make a backup of previous terminalrc
    bu terminalrc
    mv -f .terminalrc.tmp terminalrc
    cd $current_dir
}

# Extracted from https://raimue.blog/2013/01/30/tmux-update-environment
# Ready for test
# Wrapper for tmux to add additional parameters/commands to tmux
function tmux() {
    local tmux=$(type -fp tmux)
    case "$1" in
        update-environment|update-env|env-update)
            local v
            while read v; do
                if [[ $v == -* ]]; then
                    unset ${v/#-/}
                else
                    # Add quotes around the argument
                    v=${v/=/=\"}
                    v=${v/%/\"}
                    eval export $v
                fi
            done < <(tmux show-environment);;
        *)
            $tmux "$@";;
    esac
}

# Add a new pane in your tmux window with the command given as parameter
# Ready for test
function tmw {
   tmux split-window -dh "$*"
}

# Syntax highlighting cat
function hcat {
   # Use moria as theme - supress errors
   highlight $1 -s moria -O xterm256 2> /dev/null

   if [[ $? != 0 ]]; then
       cat $1 # Just cat the file without syntax highlighting
   fi
}

# Add automatically the hostname to tmux window name
case "$TERM" in
    screen*)
        PROMPT_COMMAND="printf '\033k$(hostname)\033\\';"${PROMPT_COMMAND};;
esac

# Help file
function envhelp() {
   echo "List of aliases in ds_environment.sh:"
   echo "- General aliases:"
   echo "  * rm:      Remove file and prompt before removal. Use \rm if you want to use the original rm command without -i parameter."
   echo "  * cp:      Copy file and prompt before overwrite. Use \cp if you want to use the original cp command without -i parameter."
   echo "  * mv:      Move file and prompt before overwrite. Use \mv if you want to use the original mv command without -i parameter."
   echo "  * hh:      Get last 50 commands in history file"
   echo "  * h:       Get all commands in history file"
   echo "  * r:       Repeat last command"
   echo "  * hg:      Search for a word in history. Example: 'hg service' will display all commands in history with the word service"
   echo "  * pss:     Search for process in the process list - displays id and command. (or use pgrep for just the id). Example: pss dmn"
   echo "  * ll:      List all files as a list, print sizes in human readable format, sort by modification time and reverse order."
   echo "  * lx:      List all files as a list sorted by extension"
   echo "  * lc:      List all files as a list sorted by/show change time, most recent last"
   echo "  * tree:    List contents of directories in a tree-like format: print username, size of each file in human readable format. Colorization on."
   echo "  * cd:      Function to save all directories. Use 'cd --' to display a list (with number) of each directory. Use cd -<num> to go directly to the directory with number <num> from the list"
   echo "  * cdi:     Change directory to /icas"
   echo "  * ..:      Change directory to .."
   echo "  * .2:      Change directory to ../.."
   echo "  * .3:      Change directory to ../../.."
   echo "  * .4:      Change directory to ../../../.."
   echo "  * mnt:     Display mounted devices in human readable format."
   echo "  * sprompt: Change Linux prompt to display memory statistics, number of cores, load average, current time, current user and hostname. Letters in red color for user root."
   echo "  * dprompt: Change Linux prompt to default. It displays current user and hostname. Letters in red color for user root."
   echo "  * grep:    Use grep to display matched strings in color. Use \grep if you want to use the original grep command without colours."
   echo "  * ff:      Find file in this directory/subdirectories. Example: 'cd /etc; ff ifcfg' Displays all files with ifcfg in file name."
   echo "  * ffls:    Same as ff but perform ls to all found files"
   echo "  * grepr:   grep recursively for a string ($1) in directory ($2) (if not given, current dir). Example: cds; grepr heartbeat"
   echo "  * grepp:   Simulate 'grep -p' from True64, ignoring case during search. The search pattern is colorized. Example: 'grepp TSS dep_cmd.xml'"
   echo "  * bu:      Backups a file ending with .backup-<dayMonthYear_HourMinuteSecond>. Example: cdkick; bu itec_releases"
   echo "  * st:      Sets the title of the Tab in your terminal to the text given as parameter."
   echo "  * sshup:   Updates known_hosts file from ~/.ssh/known_hosts with the given hostnames. Example: sshup icocmd13 icocmd23"
   echo "  * xcs:     Switch Xfce4 color palette"
   echo "  * xcolors: Display all 256 colors n xterm"
   echo "  * rcolors: Display contents file removing color codes (^[[37;5, ^[[0m, ...). Example: rcolors myfile_with_color_codes.log"
   echo "  * tmw:     Add a new pane in your tmux window with the command given as parameter"
   echo "  * pna:      Get the network Addresses used by the process from the given parameter. Example: pna DDS (will give the addresses used by all DDS processes excluding localhost addresses)"
   echo "  * path :    Displays the contents of PATH environment variable line by line"
   echo "  * envhelp:  Displays the list of all aliases and functions"
   echo "  * hcat:     Highlight cat. Displays the contents of a file with syntax highlighting"
}
