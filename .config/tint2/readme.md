# TINT2

tint2 is a simpel panel/taskbar for X window managers. It was mad for openbox,
but can be used in other WMs.

Usage

```
> tint2 -c <config_file> => Specifies config file
> tint2 -v               => Prints version information
> tint2 -h               => Displays help
```

## Configuration

You can edit the config file or use the graphical interface `tin2conf`

After manual change, you can reload the configuration with `killall -SIGUSR1
tint2`


See [link](https://gitlab.com/o9000/tint2/blob/master/doc/tint2.md) for all
configuration options.

What can be configured?

* Background and borders
* Gradients
* Gradient types
* Items (Launcher, taskbar, clock, systray...)
* Position, size
* Config for each item
