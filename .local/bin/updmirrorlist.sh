#!/usr/bin/env sh

# Description: Updates the arch /etc/pacman.d/mirrorlist with reflector
#    Get only mirrors in Germany
#    Get only mirrors updated in the last 24 hours
#    User protocol https
#    Sort by speed (rate)
echo "Updating mirrorlist..."
sudo reflector --country Germany --age 24 --protocol https --sort rate \
               --save /etc/pacman.d/mirrorlist
echo "Saved $(grep -c https /etc/pacman.d/mirrorlist) mirrors"
notify-send "PACMAN MIRRORLIST" "Mirror List updated"
