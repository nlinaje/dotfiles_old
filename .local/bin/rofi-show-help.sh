#!/usr/bin/env sh

# Credits: thugcee
# https://www.reddit.com/r/bspwm/comments/aejyze/tip_show_sxhkd_keybindings_with_fuzzy_search/

# Displays SXHKD Key bindings with explanation in rofi

hlp_file="${HOME}/.config/sxhkd/sxhkdrc"
awk '/^[a-zA-Z]/ && last {print $0,"\t",last} {last=""} /^#/{last=$0}' "$hlp_file" | \
   column -t -s '	' | rofi -dmenu -i -width 1000 -p "SXHKD Keys Help"

# Notes: In column -t -s '   ' we put a real tabulator with <^V-tab>. The other
# way to do this is _s $'\t' (which is not real POSIX). If you want to use the
# latter, change your shebang to bash
